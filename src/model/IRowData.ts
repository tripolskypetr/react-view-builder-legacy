export interface IRowData {
    id: string | number;
}

export default IRowData;
