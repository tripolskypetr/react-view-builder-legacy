export enum ColumnType {
    Text = 'text-cell',
    Action = 'action-cell',
    CheckBox = 'checkbox-cell',
    Custom = 'custom-cell',
}

export default ColumnType;
