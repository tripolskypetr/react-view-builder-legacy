export interface IProgressSlot {
    maxPercent?: number;
    showPercentLabel?: boolean;
    value: number;
}

export default IProgressSlot;
