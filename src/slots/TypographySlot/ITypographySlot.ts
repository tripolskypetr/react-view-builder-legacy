import { CSSProperties } from "react";

export interface ITypographySlot {
    value: string;
    placeholder: string;
    typoVariant: any;
    style?: CSSProperties;
}

export default ITypographySlot;
